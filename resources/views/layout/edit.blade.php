@extends('layout.master')
@section('title')
modifire
@endsection
@section('contenet')
<style>
    .row{
        display: flex;
        justify-content: center;

    }
</style>
<div class="card">
    <div class="row my-4">
        <div class="col-md-8 max-auto">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h1>modifier une publication</h1>
            <form action="{{route('update',$post->slag)}}" method="post">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="#" class="form-label">Title</label>
                    <input type="text" class="form-control" value="{{$post->title}}" name="title" placeholder="title">
                </div>
                <div class="mb-3">
                    <label for="#" class="form-label">Image</label>
                    <input type="file" class="form-control" name="image" placeholder="title">
                </div>
                <div class="mb-3">
                    <label for="#" class="form-label">Example textarea</label>
                    <textarea class="form-control" name="para" rows="3">{{$post->para}} </textarea>
                </div>
                <button class='btn btn-primary'>Ajouter</button>
                </form>
        </div>
    </div>
</div>
@endsection
