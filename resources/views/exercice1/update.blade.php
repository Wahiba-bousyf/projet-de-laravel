@extends('exercice1.master')
@section('title')
update {{$client->nom}}
@endsection
<style>
    .row{
        display: flex;
        justify-content: center;
    }
    .card{
        width: 50%;
        margin-left: 25%;
        margin-top: 4%;
    }
</style>
@section('contenet')
<div class="card">
    <div class="row my-4">
        <div class="col-md-8 max-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h1>hello to update client</h1>
            <form action="{{route('update',$client->id)}}" method="post">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="#" class="form-label">Name complite</label>
                    <input type="text" class="form-control" name="nom" value="{{$client->nom}}" placeholder="wahiba bousyf">
                </div>
                <div class="mb-3">
                    <label for="#" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" value="{{$client->email}}" placeholder="wahiababousyf@gmail.com">
                </div>
                <div class="mb-3">
                    <label for="#" class="form-label">Descreption</label>
                    <textarea class="form-control" name="des"  rows="3">{{$client->des}}</textarea>
                </div>
                <button class='btn btn-primary'>Add</button>
                </form>
        </div>
    </div>
</div>
@endsection
