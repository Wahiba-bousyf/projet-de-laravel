@extends('exercice1.master')
@section('title')
home page
@endsection
<style>
    .bt{
        display: flex;
        justify-content: space-evenly;
    }
    .card{
        margin-top: 15px;
    }
</style>
@section('contenet')
<div class="container">
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
<h1>hello to home page</h1>
    <div class="crad-group">
        <div class="row my-5">
            @foreach ($clients as $client)
                <div class="col-md-4">
                    <div class="card h-200">
                        <div class="card-body">
                            <h1>{{$client->nom}}</h1>
                            <p>{{Str::limit($client->des,10)}}</p>
                            <span class="bt">
                                <a href="{{route('more',$client->id)}}"><button class="btn btn-primary">More</button></a>
                                <a href="{{route('edit',$client->id)}}"><button class="btn btn-warning">edit</button></a>
                                <form action="{{route('delete',$client->id)}}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger">delete</button>
                                </form>
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="d-flex-justify-content-center my-4">
        {{$clients->links()}}
    </div>
</div>
@endsection
