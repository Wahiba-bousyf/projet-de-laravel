@extends('exercice1.master')
@section('title')
{{$client->nom}}
@endsection
<style>
    .bt{
        display: flex;
        justify-content: space-evenly;
    }
</style>
@section('contenet')
<div class="container">
    <div class="card-group">
        <div class="row my-5">
                <div class="col-md-8">
                    <div class="card h-200">
                        <div class="card-body">
                            <h1>{{$client->nom}}</h1>
                            <p>{{$client->email}}</p>
                            <p>{{$client->des}}</p>
                            <span class="bt">
                            <a href="{{route('home')}}"> <button class="btn btn-success" >back</button></a>
                            <a href="#"><button class="btn btn-warning">edit</button></a>
                            <form id='{{$client->id}}' action="{{route('delete',$client->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button class='btn btn-danger' onclick="event.preventDefault(); if(confirm('etes vous daccorde pour la supprission?')) document.getElementById({{$client->id}}).submit();">delete</button>
                            </form>
                        </span>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
