@extends('layout.master')
@section('title')
    {{$post->title}}
@endsection
<style>
    .bt{
        display: flex;
        justify-content: space-evenly;
    }
    .card{
        width: 600px;
    }
</style>
@section('contenet')
    <div class="container">
        <div class="card-group">
            <div class="row my-5">
                    <div class="col-md-4">
                        <div class="card h-200">
                            <img src="{{asset('./uploads/'.$post->image)}}" alt="">
                            <div class="card-body">
                                <h1>{{$post['title']}}</h1>
                                <p>{{$post->para}}</p>
                                <span class="bt">
                                <a href="{{route('home')}}"> <button class="btn btn-success" >back</button></a>
                                <a href="{{route('edit',$post->slag)}}"><button class="btn btn-warning">edit</button></a>
                                <form action="{{route('delete',$post->slag)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger" type="submit">delete</button>
                                </form>
                            </span>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

