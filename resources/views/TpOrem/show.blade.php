@extends('TpOrem.master')
@section('contenu')
<style>
    .card{
        width: 50%;
        margin-top: 50px;
    }
    .ele{
        margin-left: 30px;
    }
    .btn{
        margin-left: 60%;
        margin-top: -40px;
    }
</style>
<div class="container">
    <div class="card">
        <div class="ele">
        <h1>Détails</h1>
        <h5>Nom Complete :</h5>{{$stagiaire->nom}}
        <h5>Genre :</h5>{{$stagiaire->genre}}
        <h5>Note :</h5>{{$stagiaire->note}}
        <h5>Groupe :</h5>{{$stagiaire->groupe}}
        <h5>Created at :</h5>{{$stagiaire->created_at}}
        <h5>updtaed at :</h5>{{$stagiaire->updated_at}}
        <a href="{{route('home')}}"><button class="btn btn-success">back</button></a>
    </div>
</div>
</div>
@endsection
