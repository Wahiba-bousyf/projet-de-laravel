@extends('TpOrem.master')
@section('contenu')
<style>
    .container{
        margin-top: 90px;
    }
    .btn{
        display: flex;
        justify-content: space-between;
    }
    table{
        margin-top: 20px;
    }
    img{
        width: 60px;
    }
</style>
<div class="container">
    @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif
        <h1>Bonjour a tous</h1>
        <a href="{{route('create')}}"><button class="btn btn-primary">ajouter un stagiaiare</button></a>
        <table class="table table-dark table-hover">
            <thead>
                <th>ID</th>
                <th>Image</th>
                <th>Nom Complete</th>
                <th>Genre</th>
                <th>Date Naissance</th>
                <th>Note</th>
                <th>Groupe</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($stagiaires as $stagiare )
                <tr>
                    <td>{{$stagiare->id}}</td>
                    <td><img src="{{asset('./uploads/'.$stagiare->image)}}" alt=""></td>
                    <td>{{$stagiare->nom}}</td>
                    <td>{{$stagiare->genre}}</td>
                    <td>{{$stagiare->date}}</td>
                    <td>{{$stagiare->note}}</td>
                    <td>{{$stagiare->groupe}}</td>
                    <td>
                        <span class="btn">
                            <a href="{{route('show',$stagiare->id)}}"><button class='btn btn-success'>Afficher</button></a>
                            <a href="{{route('edit',$stagiare->id)}}"><button class="btn btn-warning">Modifier</button></a>
                            <form id={{$stagiare->id}} action="{{route('delete',$stagiare->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button onclick="event.preventDefault(); if(confirm('etes vous daccorde pour la supprission?')) document.getElementById({{$stagiare->id}}).submit();" class="btn btn-danger">suprimer</button></td>
                            </form>
                        </span>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>
@endsection
