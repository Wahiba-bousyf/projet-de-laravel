@extends('TpOrem.master')
@section('title')
modifier
@endsection
<style>
    .card{
        margin-top: 15px;
        width: 60%;
    }
    .container{
        display: flex;
        justify-content: center;
    }
    .con{
        margin-left: 10%;
    }
</style>
@section('contenu')
<dv class="container">
    <div class="card">
        <div class="row my-4">
            <div class="col-md-8 max-auto">
                <div class="con">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h1>Nouveau stagiaire</h1>
                <form action="{{route('update',$stagaire->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="mb-3">
                        <label for="#" class="form-label">nom complet</label>
                        <input type="text" class="form-control" name="nom" value='{{$stagaire->nom}}' placeholder="wahiba bousyf">
                    </div>
                    <div class="mb-3">
                        <label for="#">Image</label><br/>
                        <input type="file" name='image'>
                    </div>
                    <div class="mb-3">
                        <label for="#" class="form-label">Genre :</label>
                        <input type="radio" name="genre" value="femme">F
                        <input type="radio" name="genre" value="masculin">M
                    </div>
                    <div class="mb-3">
                        <label for="#" class="form-label">Date de Naissance</label>
                        <input type="date" class="form-control" value='{{$stagaire->date}}' name="date">
                    </div>
                    <div class="mb-3">
                        <label for="#" class="form-label">Note</label>
                        <input type="text" class="form-control" name="note" value='{{$stagaire->note}}' placeholder="19">
                    </div>
                    <div class="mb-3">
                        <label for="#" class="form-label">Groupe</label>
                        <input type="text" class="form-control" name="groupe" value='{{$stagaire->groupe}}' placeholder="1">
                    </div>
                    <button class='btn btn-primary'>Add</button>
                    </form>
            </div>
        </div>
        </div>
    </div>
</dv>
@endsection
