@extends('layout.master')
@section('title')
home page
@endsection
<style>
    .bt{
        display: flex;
        justify-content: space-evenly;
    }
</style>
@section('contenet')
@if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success')}}
    </div>
@endif
<div class="container">
    <div class="crad-group">
        <div class="row my-5">
            @foreach ($posts as $post)
                <div class="col-md-4">
                    <div class="card h-200">
                        <img src="{{asset('./uploads/'.$post->image)}}" alt="" />
                        <div class="card-body">
                            <h1>{{$post->title}}</h1>
                            <p>{{Str::limit($post->para,10)}}</p>
                            <span class="bt">
                                <a href="{{route('post.show',$post->id)}}"><button class="btn btn-primary">More</button></a>
                                <a href="{{route('edit',$post->slag)}}"><button class="btn btn-warning">edit</button></a>
                                <form id={{$post->id}} action="{{route('delete',$post->slag)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button onclick="event.preventDefault(); if(confirm('etes vous daccorde pour la supprission?')) document.getElementById({{$post->id}}).submit();" class="btn btn-danger" type="submit">delete</button>
                                </form>
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="d-flex-justify-content-center my-4">
            {{$posts->links()}}
        </div>
    </div>
</div>
@endsection
