<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TpR;
use App\Models\TpM;

class TpController extends Controller
{
    public function index()
    {
        $stagaires=TpM::get();
        return view('TpOrem.home')->with([
            'stagiaires'=>$stagaires
        ]);;
    }
    public function create()
    {
        return view('TpOrem.create');
    }
    public function store(TpR $request)
    {
        if($request->has('image')){
            $file=$request->image;
            $image_n=time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'),$image_n);
        };
        TpM::create([
            'nom'=>$request->nom,
            'image'=>$image_n,
            'genre'=>$request->genre,
            'date'=>$request->date,
            'note'=>$request->note,
            'groupe'=>$request->groupe,
        ]);
        return redirect()->route('home')->with([
            'success'=>'client est bien ajouter'
        ]);
    }
    public function show($id)
    {
        $stagiare=TpM::find($id);
        return view ('TpOrem.show')->with([
            'stagiaire'=>$stagiare
        ]);
    }
    public function edit($id)
    {
        $stagiaire=TpM::find($id);
        return view('TpOrem.edit')->with([
            'stagaire'=>$stagiaire
        ]);
    }
    public function update(TpR $request,$id)
    {
        $stagiaire=TpM::where('id',$id)->first();
        if($request->has('image')){
            $file=$request->image;
            $image_n=time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'),$image_n);
            $stagiaire->image=$image_n;
        }
        $stagiaire->update([
            'nom'=>$request->nom,
            'image'=>$stagiaire->image,
            'genre'=>$request->genre,
            'date'=>$request->date,
            'note'=>$request->note,
            'groupe'=>$request->groupe,
        ]);
        return redirect()->route('home')->with([
            'success'=>'le stagaire est modifier'
        ]);
    }
    public function delete($id)
    {
        $stagaire=TpM::where('id',$id)->first();
        $stagaire->delete();
        return redirect()->route('home')->with([
            'success'=>'le stagiaire est suprimer'
        ]);
    }
}
