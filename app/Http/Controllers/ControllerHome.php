<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactR;

class ControllerHome extends Controller
{
    public function myfunction()
    {
        $posts=Post::latest()->paginate(3);
        return view('home')->with([
            'posts'=>$posts
        ]);
    }
    public function show($id)
    {
        $post=Post::find($id);
        return view('show')->with([
            'post'=>$post
        ]);
    }
    public function create()
    {
        return view('layout.create');
    }
    public function store(ContactR $request)
    {
        // $this->validate($request,[
        //     'title'=>'required|min:3|max:100',
        //     'para'=>'required|unique:posts|min:3|max:300',
        //     'image'=>'required|mimes:png,jpg,jpeg',
        // ]);
        if($request->has('image')){
            $file=$request->image;
            $image_n=time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'),$image_n);
        }
        Post::create([
            'title'=>$request->title,
            'para'=>$request->para,
            'image'=>$image_n,
            'slag'=>Str::slug($request->title),
        ]);
        return redirect()->route('home')->with([
            'success'=>'article est bien ajouter'
        ]);
        // $post=new Post();
        // $post->title=$request->title;
        // $post->para=$request->para;
        // $post->slag=Str::slug($request->title);
        // $post->image="https://via.placeholder.com/640x480.png/001199?text=et";
        // $post->save();
    }
    public function edit($slag)
    {
        $posts=Post::where('slag',$slag)->first();
        return view('layout.edit')->with([
            'post'=>$posts
        ]);
    }
    public function update(Request $request ,$slag)
    {
        $post=Post::where('slag',$slag)->first();
        $post->update([
            'title'=>$request->title,
            'para'=>$request->para,
            'slag'=>Str::slug($request->title),
            'image'=>"https://via.placeholder.com/640x480.png/001199?text=et",
        ]);
        return redirect()->route('home')->with([
            'success'=>'article bien modifeir'
        ]);
    }
    public function delete($slag)
    {
        $post=Post::where('slag',$slag)->first();
        $post->delete();
        return redirect()->route('home')->with([
            'success'=>'article est suprimer'
        ]);
    }
}
