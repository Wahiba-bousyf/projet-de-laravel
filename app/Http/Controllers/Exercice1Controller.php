<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\PostRequest;
use App\Models\Post2;

class Exercice1Controller extends Controller
{
    public function index()
    {
        $clients=Post2::latest()->paginate(3);
        return view('exercice1.home')->with([
            'clients'=>$clients
        ]);
    }
    public function create()
    {
        return view('exercice1.create');
    }
    public function store(PostRequest $request)
    {
        Post2::create([
            'nom'=>$request->nom,
            'email'=>$request->email,
            'des'=>$request->des,
        ]);
        return redirect()->route('home')->with([
            'success'=>'client est bien ajouter'
        ]);
    }
    public function more($id)
    {
        $client=Post2::find($id);
        return view('exercice1.more')->with([
            'client'=>$client
        ]);
    }
    public function edit($id)
    {
        $client=Post2::find($id);
        return view('exercice1.update')->with([
            'client'=>$client
        ]);
    }
    public function update(PostRequest $request,$id)
    {
        $client=Post2::where('id',$id)->first();
        $client->update([
            'nom'=>$request->nom,
            'email'=>$request->email,
            'des'=>$request->des,
        ]);
        return redirect()->route('home')->with([
            'success'=>'client est modifier'
        ]);
    }
    public function delete($id){
        $client=Post2::where('id',$id)->first();
        $client->delete();
        return redirect()->route('home')->with([
            'success'=>'client est suprimer'
        ]);
    }
}
