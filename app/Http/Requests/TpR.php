<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TpR extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nom'=>'required|min:4|max:20',
            'genre'=>'required',
            'date'=>'required|date',
            'note'=>'required|min:0|max:20',
            'groupe'=>'required|min:1|max:10',
        ];
    }
}
