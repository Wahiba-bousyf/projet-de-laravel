<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TpM extends Model
{
    protected $table='stagiaire';
    protected $fillable=['nom','image','genre','date','note','groupe'];
}
